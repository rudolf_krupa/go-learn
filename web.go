package main

import (
	"encoding/json"
	"fmt"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"path/filepath"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func GetCurrentDirectory() string {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	return filepath.Dir(ex)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func LoadTemplate(templateFile string) string {
	exPath := GetCurrentDirectory()
	data, err := ioutil.ReadFile(exPath + "/static/" + templateFile)
	if (err != nil) {
		log.Panic(err)
	}
	return string(data)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func indexPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, LoadTemplate("index.html"))
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func removeConnection(hash string, conn net.Conn) {
	fmt.Println("delete connection: " + hash)

	delete(connections, hash)
	conn.Close()

	// send list of all connections to all
	sendListOfConnections()
}

func formatMessageText(senderHash string, receiverHash string, message []byte) []byte {
	messageItem := Message{Sender: string(senderHash), Receiver: string(receiverHash), Message: string(message)}
	messageJson, errJson := json.Marshal(&messageItem)
	if errJson != nil {
		log.Panic(errJson)
	}

	return messageJson
}

func sendMessage(conn net.Conn, message []byte) {
	err := wsutil.WriteServerMessage(conn, ws.OpText, message)
	if err != nil {
		log.Panic(err)
	}
}

func sendListOfConnections() {
	for _, connection := range connections {

		items := Connections{Connections: getConnections()}
		connectionsJson, err := json.Marshal(&items)
		if err != nil {
			log.Panic(err)
		}
		//fmt.Println(string(connectionsJson))
		sendMessage(connection, connectionsJson)
	}
}

func createConnection(w http.ResponseWriter, r *http.Request) {

	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		log.Panic(err)
	}

	//
	hash := RandStringBytes(12)
	connections[hash] = conn

	fmt.Printf("new connection: %s\n", hash)

	welcomeMessage := fmt.Sprintf("*welcome connection:* **%s**", hash)

	sendMessage(conn, formatMessageText(hash, hash, []byte(welcomeMessage)))

	// send connection hash
	hashSendJson := fmt.Sprintf("{\"hash\":\"%s\"}", hash)
	sendMessage(conn, []byte(hashSendJson))

	// send list of all connection to all
	sendListOfConnections()

	go func() {
		defer removeConnection(hash, conn)

		for {
			msg, _, err := wsutil.ReadClientData(conn)
			if err != nil {
				fmt.Printf("%s - %s\n", hash, err)
				return
			}

			// send
			parseAndSendMessage(msg)
		}
	}()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var connections = make(map[string]net.Conn)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type Message struct {
	Sender string `json:"sender"`
	Receiver string `json:"receiver"`
	Message string `json:"message"`
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type Connection struct {
	Hash string `json:"hash"`
}

type Connections struct {
	Connections []string `json:"connections"`
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type Result struct {
	Status string `json:"status"`
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type Request struct {
	Sender		string `json:"sender"`
	Message     string   `json:"message"`
	Connections []string `json:"connections"`
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func getConnections() []string {
	hashes := []string{}
	for key := range connections {
		hashes = append(hashes, key)
	}

	return hashes
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func hasConnectionByHash(hash string) bool {
	_, ok := connections[hash]
	return ok
}

func getConnectionByHash(hash string) net.Conn {
	connection, _ := connections[hash]
	return connection
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func parseAndSendMessage(body []byte) {
	var request Request
	json.Unmarshal(body, &request)

	//fmt.Print(request)
	//fmt.Print(request.Message)
	//fmt.Print(request.Connections)

	if len(request.Connections) > 0 && len(request.Message) > 0 {
		for _, receiverHash := range request.Connections {

			fmt.Printf("%s - %s\n", receiverHash, request.Message)

			//fmt.Println(hash)
			if hasConnectionByHash(receiverHash) {
				sendMessage(getConnectionByHash(receiverHash), formatMessageText(request.Sender, receiverHash, []byte(request.Message)))
			}
		}
	}
}

func main() {

	const PORT = 8080

	// routes
	router := httprouter.New()

	router.GET("/", indexPage)

	router.GET("/ws", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		createConnection(w, r)
	})

	router.GET("/connections", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

		// todo: connections json format change
		items := Connections{Connections: getConnections()}
		connectionsJson, err := json.Marshal(&items)
		if err != nil {
			log.Panic(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		w.Write(connectionsJson)
	})

	router.POST("/send-message", func(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {

		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			panic(err)
		}

		parseAndSendMessage(body)

		// to do status
		result := Result{Status: "ok"}
		jsonResult, err := json.Marshal(&result)
		if err != nil {
			log.Panic(err)
		}
		//fmt.Println(string(jsonResult))

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		w.Write(jsonResult)
	})

	//
	portString := fmt.Sprintf(":%d", PORT)
	error := http.ListenAndServe(portString, router)
	fmt.Printf(fmt.Sprintf("%s:%d\n", "http://localhost", PORT))
	if (error != nil) {
		log.Fatal(error)
	}
}
