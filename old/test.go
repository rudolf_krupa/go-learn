package main

import "fmt"

func main() {

	a := []string{
		"hello-",
		"world",
		"test",
	}
	b := []string{
		"goodbye",
		"world",
	}

	copy(a, b)

	fmt.Println(a)
	fmt.Println(b)

	//
	var c string = "test 1"
	var d string = "test 2"

	fmt.Println(c + " - " + d)

	var x int;
	x = 7;
	//fmt.Println(x);

	var y string;
	y = " testsfasdf";

	fmt.Printf("%v %s \n", x, y);

	//
	var sample []byte
	sample = []byte("bytetest")

	fmt.Println(string(sample))
}