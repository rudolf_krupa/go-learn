package old

import (
	"fmt"
	"sync"
	/* "time" */
	/* "math/rand" */
)

func f(from string) {
	for i := 0; i < 3; i++ {
		fmt.Println(from, ":", i)
	}
}

func main() {

	/*
	go f("direct")

	go func(msg string) {
		fmt.Println(msg)
	}("going")

	go f("goroutine")

	fmt.Scanln()
	fmt.Println("done")
	*/

	/*
	//
	c1 := make(chan float64, 1)
	c2 := make(chan int, 1)

	go func() {
		// simulate spending time to do work to get answer
		time.Sleep(time.Duration(rand.Intn(2000)) * time.Millisecond)
		c1 <- 1.2
	}()

	go func() {
		time.Sleep(time.Duration(rand.Intn(2000)) * time.Millisecond)
		c2 <- 34
	}()

	x := <-c1
	y := <-c2
	fmt.Println(x, y)
	*/

	var wg sync.WaitGroup
	totalNumberInstances := uint64(10000);
	for loops := uint64(1); loops <= totalNumberInstances; loops++ {
		wg.Add(1)
		go func(instanceNumber uint64, totalNumberInstances uint64) {
			//time.Sleep(time.Duration(rand.Intn(2000)) * time.Microsecond)
			fmt.Println(instanceNumber)

			defer wg.Done()
		}(loops, totalNumberInstances)
	}

	wg.Wait()
	fmt.Println("done")
}