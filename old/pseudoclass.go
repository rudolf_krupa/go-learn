package main

import (
	"fmt"
)

type PseudoClass struct {
	sampleVariable string
	secondSampleVariable string
}

func (pseudoClass *PseudoClass) sample() {
	fmt.Println("PseudoClass::sample")
}

func (pseudoClass *PseudoClass) sampleString() string {
	return "PseudoClass::sampleString"
}

func (pseudoClass *PseudoClass) getSampleVariable() string {
	return pseudoClass.sampleVariable;
}

func (pseudoClass *PseudoClass) setSampleVariable(variable string) {
	pseudoClass.sampleVariable = variable;
}

func main() {
	//pseudoClass := PseudoClass{sampleVariable:"testasdasd"}
	//fmt.Println(pseudoClass.getSampleVariable())

	pseudoClass := new(PseudoClass);
	pseudoClass.sample()
	fmt.Println(pseudoClass.sampleString())
}

